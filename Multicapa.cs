﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perceptron
{
    class Multicapa
    {
        static void Main(string[] args)
        {

            bool sw = true;

            while (sw)
            {
                sw = false;
                Perceptron p = new Perceptron(2, new int[] { 2, 1 });
                Console.WriteLine("----------------");
                Console.WriteLine("E1: 1 E2: 1: " + p.Salidas(new float[] { 1, 1 })[0]);
                Console.WriteLine("E1: 1 E2: 0: " + p.Salidas(new float[] { 1, 0 })[0]);
                Console.WriteLine("E1: 0 E2: 1: " + p.Salidas(new float[] { 0, 1 })[0]);
                Console.WriteLine("E1: 0 E2: 0: " + p.Salidas(new float[] { 0, 0 })[0]);

                if (p.Salidas(new float[] { 1, 1 })[0] != 0)
                {
                    sw = true;
                }
                if (p.Salidas(new float[] { 1, 0 })[0] != 1)
                {
                    sw = true;
                }
                if (p.Salidas(new float[] { 0, 1 })[0] != 1)
                {
                    sw = true;
                }
                if (p.Salidas(new float[] { 0, 0 })[0] != 0)
                {
                    sw = true;
                }


            }
            Console.ReadKey();

        }
    }

    class Perceptron
    {
        public List<Capa> red = new List<Capa>();
        public int[] neuronasPorCapa;

        public Perceptron(int entradasExternas, int[] NeuronasPorCapa)
        {
            Random r = new Random();
            neuronasPorCapa = NeuronasPorCapa;

            red.Add(new Capa(entradasExternas, NeuronasPorCapa[0], r));
            for (int i = 1; i < NeuronasPorCapa.Length; i++)
            {
                red.Add(new Capa(NeuronasPorCapa[i - 1], NeuronasPorCapa[i], r));
            }

        }

        public float[] Salidas(float[] entradasExternas)
        {
            red[0].Salidas(entradasExternas);
            for (int i = 1; i < red.Count; i++)
            {
                red[i].Salidas(red[i - 1].Salida);
            }

            return red[red.Count - 1].Salida;
        }

    }

    class Capa
    {
        public List<Neuron> capa = new List<Neuron>();
        public float[] Salida;
        Random r;
        public Capa(int Entradas, int NumeroNeuronas, Random R)
        {
            r = R;

            for (int i = 0; i < NumeroNeuronas; i++)
            {
                capa.Add(new Neuron(Entradas, r));
            }
        }

        public void Salidas(float[] entradas)
        {
            float[] f = new float[capa.Count];
            for (int i = 0; i < capa.Count; i++)
            {
                f[i] = capa[i].Salida(entradas);
            }
            Salida = f;
        }

    }

    class Neuron
    {
        float[] PesosAnteriores;
        float UmbralAnterior;
        //Peso = PesoAnterior * Tasa de aprendizaje * error * entrada
        public float[] pesos;
        public float umbral;
        public float tasaDeAprendizaje = 0.3f;
        Random r;

        public Neuron(int Nentradas, Random R, float TasaAprendizaje = 0.3f)
        {
            r = R;
            tasaDeAprendizaje = TasaAprendizaje;
            pesos = new float[Nentradas];
            PesosAnteriores = new float[Nentradas];
            Aprender();
        }

        public void Aprender()
        {
            for (int i = 0; i < PesosAnteriores.Length; i++)
            {
                PesosAnteriores[i] = Convert.ToSingle(r.NextDouble() - r.NextDouble());
            }

            UmbralAnterior = Convert.ToSingle(r.NextDouble() - r.NextDouble());
            pesos = PesosAnteriores;
            umbral = UmbralAnterior;

        }

        public void Aprender(float[] entradas, float salidaEsperada)
        {
            if (PesosAnteriores == null)
            {
                Console.WriteLine("Error en los pesos");
                return;
            }
            float Error = salidaEsperada - Salida(entradas);
            for (int i = 0; i < pesos.Length; i++)
            {
                pesos[i] = PesosAnteriores[i] + tasaDeAprendizaje * Error * entradas[i];

            }
            umbral = UmbralAnterior + tasaDeAprendizaje * Error;

            PesosAnteriores = pesos;
            UmbralAnterior = umbral;


        }

        public float Salida(float[] entradas)
        {
            return Sigmoud(Neurona(entradas));
        }
        float Neurona(float[] entradas)
        {
            float Sum = 0;
            for (int i = 0; i < pesos.Length; i++)
            {
                Sum += entradas[i] * pesos[i];
            }
            Sum += umbral;

            return Sum;
        }
        float Sigmoud(float n)
        {
            return (n > 0) ? 1 : 0;
        }


    }
}